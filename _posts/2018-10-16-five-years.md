---
layout: post
title:  "Facebook田渊栋：博士五年之后五年的总结"
date:   2018-10-16 23:00:00
categories: 田渊栋
tags: 田渊栋, 博士
excerpt: 值得深思的好文章，现任Facebook人工智能研究院（FAIR LAB）研究员/研究经理，前Google无人驾驶部门软件工程师。本科和硕士毕业于上海交通大学，并在卡内基梅隆大学获得机器人方向博士学位。
mathjax: true
---

[Facebook田渊栋：博士五年之后五年的总结](https://mp.weixin.qq.com/s?__biz=MzAxMzc2NDAxOQ==&mid=2650369213&idx=1&sn=9045f9948df77cb722ef2873d5466f40&chksm=83905861b4e7d177f0e320ca5497615a2ce777556e9c6d59503398d1dc2bc824a4adb5af60a2&mpshare=1&scene=1&srcid=1016J1NUjbZDjbPIH5PyFk49#rd)

* content
{:toc}

>编者按：田渊栋，在硅谷年轻一代华人工程师和计算机科研工作者中具有不小的知名度。身为Facebook人工智能研究院研究员的他在专注科研的同时，一直笔耕不辍。他的文章具有深邃的思想，又兼具可读性。五年前，他在知乎上分享的文章《[博士五年总结](https://zhuanlan.zhihu.com/p/30656493)》曾引起关注；五年后，他再用《博士五年之后五年的总结》系列文章，回顾了博士毕业五年，在硅谷从谷歌无人驾驶转到Facebook人工智能研究院的所思与所得。本文汇总了该系列文章的前言、其一、其二和其三。

![田渊栋](https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1540311926&di=12baf040b450b7a8b3c7db669f6b8664&imgtype=jpg&er=1&src=http%3A%2F%2Fn1.itc.cn%2Fimg8%2Fwb%2Frecom%2F2017%2F03%2F20%2F149001635398878036.JPEG)

# 前 言

光阴如梭，时光荏苒。博士之后又是五年，离上次的《[博士五年总结](https://zhuanlan.zhihu.com/p/30656493)》也已经有五年时间了。在繁忙的工作中，我觉得有必要再写些东西，一是分享给大家自己新的经验，二是借此机会总结思考自己五年来的成败得失，以后能更进一步。

一句话总结：<font color='red'>选对人生的职业方向，及坚持这个方向，是最重要的。</font>

我2013年毕业的时候，正赶上深度学习的大潮起步。AlexNet的文章一出，整个CMU机器人系做视觉的组里，许多人抱持怀疑态度，觉得这个一定是实验出了什么问题，或者代码写错了，或者是因为过拟合。我当时正在折腾我自己多层组合以矫正图像扭曲的理论，看到多层神经网络能有效果非常兴奋，还发了信给Alex讨论他开源代码的细节。我和我导师讨论说想去做这个，我导师一个劲地摇头，以他一贯反对机器学习的态度，觉得这完全不靠谱——但一个快毕业的博士，应该有自己的判断。

当然，作为学生仍然会被现实问题所左右。拿了几个offer，其中就有硅谷做深度学习的职位，可最后还是没能坚持到底，还是去了谷歌无人车组。刚进组时满怀热情，眼界大开，发现工业界的问题还可以用算法之外的办法处理，颇有新鲜感；但到后来，越来越觉得这不是我想要走的路。每天工作时，看到一篇一篇新出的深度学习的文章，看着各类任务的性能一天天地变好，想着自己每天晚上的业余民科理论研究，厌倦于每天的小修小补还有对变量命名和接口设计的纠结，憋屈于想做的方向得不到支持——正是这每一天每一晚在脑中回响的声音，让自己明白，自己并不属于这里。

不属于的话，就走吧。

当时作这个决定非常难，家里反对，毕竟无人车听上去高大上，毕竟谷歌工资高有保障，毕竟我们仍在为生计努力，没有那么多自由。我充分发挥小说的想像力，认真写了跳槽之后可能的结局，上中下三条，惨的非常惨。我写完之后给老婆看，我说我们好好讨论下，老婆说可行。

今天，在Facebook人工智能研究院已经快四年了，回想起来，这次跳槽是非常成功的。我充分发挥了自己的能力，选对了课题，做成了我想做的事情，研究和工程都有成果，理论和应用并重，也开始积攒一定的管理经验。如果我还一直待在原来的组，可能会因为现在的无人车大火而获得相对比较高的工资待遇，但绝不会有现在的精气神，也就不会有这些文章了。

回想起来，这才是属于我的青春。


# 其 一

现在回头想来，其实<font color='green'>博士阶段是很单纯的</font>。拿着微薄的薪水，带着毕业的压力，待在一个交通闭塞的地方，在导师的指导、鼓励或是逼迫下，在周围各种牛人的压力下，花一些时间，专心地做一些东西。在这些条件下，很多人可以在一个从未尝试过的领域里面获得成功。

等到成家立业放飞自我了，往往问题就来了。再好的公司，也希望员工努力干活，而不是培养人的地方。不管表面上如何温情脉脉，最终还是要看绩效的，绩效好升职加薪，绩效不好卷铺盖走人，就这样简单。

而个人的成长方向，自始至终得要由自己负责。
- 选方向？先要控制自己阅读的入口
- <font color='red'>读什么东西，就成为什么样的人</font>。
- 读什么东西，就成为什么样的人。
- 读什么东西，就成为什么样的人。

重要的事情要说三遍。<font color='blue'>人是很容易被周围的东西影响的</font>，在现在这个手机时代尤其如此，<font color='green'>你关注谁，就会成为相似的人</font>。刷一刷各类新闻和朋友圈，几十分钟就很容易过去了。而且这些东西大多可以定制，所以<font color='red'>你的命运，可能完全取决于你，也可能完全交给你周围的人，不可不察</font>。

另外如之前我的答案里所说，书和文章可以多看，但还是要多想，多去提炼其中隐藏的主线和趋势，多去做小小的预测，形成自己的世界观和方法论，然后才自然而然会有个人的见解和想做的方向，这种思考往往是很累人的，而且在这个信息过载的时代可能大家都更习惯去接受已有的观点，并且躲在自己的安全区里面。

但在<font color='green'>日积月累之下，做或者不做这些，会让每个人最终成为不一样的人。</font>

所以多看点动脑的内容，就不会让大脑生锈。做科研一个比较好的地方是工作本身并不重复，而是一直在开拓边界，这样自然会有更多的动脑时间。在闲暇时间，我经常会多看知乎上做数学和物理的同学们的回答，最好有几个不懂的名词需要自己去查去想想，手机上有个刷Arxiv的app经常看看，看一看一些优秀的github代码，也会动手刷刷题。

在走管理职位之后，这个尤其重要。<font color='red'>“管理”这个词除了在国企之外，都是“支持”的同义词</font>，支持的意思是<font color='green'>帮人成事，事情搞完，功劳大部分是下属的，我鼓个掌说两句好听的</font>。而<font color='red'>“领导”另有个词叫“技术带头人”(tech leader)</font>，那个才是带大家冲锋在前的人。而要保证自己能做技术领导加管理的路子，<font color='green'>技术的能力是一概不能少的</font>。而参与了管理事务之后，每天开点会议，会让人产生“自己已经干了很多”的幻觉，一旦松懈下来，就再难再追上。在人工智能这个进展以月来计算的领域，要是对将来的方向不是很有见解，大家马上就不会再听你的了。


## 如何选方向？

有了选方向的点滴积累，接下来的问题是如何去选。

我们常说<font color='green'>高考难，难如千军万马过独木桥，路只有一条，如何在最短时间内获得最高的分数，这是“<font color='red'>术</font>”；其实博士毕业之后更难，面对山高水远大路小路，往哪个方向跑，那是“<font color='red'>道</font>”的问题了</font>。

然而，<font color='blue'>“道”和“术”并非完全分离，再高明的“道”，还是要“术”去贯彻的</font>。

博士阶段其实就在<font color='green'>寻找“道”中之“术”</font>。博士努力学习如何探索如何试错，如何在纷乱的线头里找到关键点，才能在将来更大的挑战前降低试错成本，在有限的时间里找到对的方向。所以读博在某种程度上是预支了人生的风险，在二十几岁的时候犯几个错不要紧，大不了浪费一年从头再来；在四十来岁的时候再拍脑袋决策，轻率犯错就会比较麻烦了。

1. 选方向，不管是科研选课题，还是VC投资，还是说选个人职业发展方向，其实是有一些通用的大原则的，比如说多从<font color='green'>第一性原理</font>出发从头解构分析，仔细思考每一步逻辑链条，多做调查，不要被三分钟热度冲昏头脑，不要挤人多的方向，从自己的独立分析和思考中找到突破口。别人都说好但自己觉得有硬伤的要三思后行，别人没注意到但自己觉得有前途的就要勇于尝试。在此之上，再发扬个人的风格喜好。
1. 选方向，立意是第一重要的。所谓“立意”是指先对这个世界有一个精炼的模型，然后在所选的方向上，思考这个模型能否给出不错的未来。强调立意，其实是要强调要找到最重要的脉络和趋势。一个博士生往往拼命干活，为一篇文章呕心沥血；但导师则可以集中在一些“一定有效”的大思路上，举重若轻地指导学生做出几篇甚至十几篇文章来。这五年来，如何从一个博士生变成导师，如何去找到能深挖大挖的金矿，是比具体技巧更重要的事情。
   - 我在《博士五年总结》里，把立意当成是一种写好文章的“术”，以归纳总结的方式，去为每篇文章或者是博士论文去寻找一个故事；但其实，它的重要性要远高于此。<font color='green'>事物的发展在大尺度上是有脉络的，是有章可循的，而历史，往往重复它自己</font>。这些，我就先不展开说了。
1. 选方向要考虑到自己的能力。定一个不那么简单但又切实可行的小目标，往往是一切的开始。不要好高骛远，也不要自卑自弃。回头看我自己的五年总结，我会觉得在博士阶段时过早去啃一些相对困难的方向，且没能经常和牛人们合作，是一个比较遗憾的地方，不然的话会走得更好。不过幸亏自己还可以在每次deadline之前迷途知返，专心做应用文章，所以整个博士阶段还算顺利。毕业前博士是一座山，毕业后博士只是一道关，过关之后，将来才有机会继续搞下去。

在这方面，成熟的人会把问题分解，按照难易排序，知道轻重缓急，知道充分交流，也知道做事节奏，在没做出什么东西之前也吃得下睡得着；而不成熟的人往往盯着一两个比较近的目标，不管三七二十一先爽过再说，而碰上比较难的问题，往往先过于乐观，然后马上放弃，缺少韧劲和思考的深度。

要去验证这个小目标。可能最重要的，是要找到第一个会出错的地方，并且修正对问题的建模。这里就是考验面对现实的勇气的时候，很多人事事都做好了，但偏偏最重要的地方不去试验，心里幻想着这事可以成，越往后拖，损失最大。举研究的例子，比如说拼命做漂亮的UI但不考虑算法会不会有效果，拼命做简单实验但不去做最可能失败的，拼命折腾编辑器设置但却不写文章。另一个例子比如说想转强化学习，在略微过一下教材之后，就可以上实验试着把东西调通，然后就可以在之前的经验之上开始思考什么方向可以发文章，然后再试。

这种有选择性的跳跃方式，可以以比较快的速度找到关键所在，并且修正自己脑中对问题的建构模型。要记得不管自己多聪明，保留”自己可能全错“的这种可能，并且经常听取别人意见或是采纳实验上的证据，不仅对于选方向是重要的，也是一个人可以持续发展的根基。


## 如何抓紧时间？

踏入工作之后，<font color='green'>时间就变得非常宝贵</font>。可能在博士阶段花一个月做的事情，工作时花两周或者一周就要能做出来。并且<font color='green'>时间变得零碎</font>，变得资深后及担任管理职位之后，开会等各种其它的事务也变多了，如果没有一个明确的方向，那很快就会陷入琐事里面去。

这方面例子非常多。一天开会，忙的时候留给自己的工作时间只有两三个小时，基本上是早上十点前及下午四点后，要是不能坐下来马上进入状态，稍微刷一刷或者聊一聊，那就啥都干不了；反过来<font color='blue'>要是心里有明确计划和目标，那就算是10分钟都可以做些事情</font>。一天有多少个十分钟碎片？很多很多，公车上，走路时，马桶上——做好计划，这些时间都可以被利用起来。古人说“<font color='green'>马上、枕上、厕上，盖惟此尤可以属思尔</font>”。一千年后，还是一样的。


## 如何坚持一个长期的方向？

我记得前一阵子在将门直播的时候，我被问到一个问题，说做研究要如何平衡短期工作和长期工作的关系，如何应对外界的压力。我觉得这个问题非常好。在博士阶段我就得要处理这个矛盾，导师想做的，未必是我自己喜欢的；我喜欢的，未必是马上能做出来的，现在工作了，仍然要处理这样的矛盾，并且因为节奏变快，责任更重，矛盾会变得更加尖锐。如同我在《博士五年总结》里面说的那样，<font color='green'>要习惯于两线作战，要有“进可攻”的长期课题，也要有“退可守”的短期课题，这样既减少对短期绩效评估的担心，又怀抱有长远的希望</font>。然而要做到这一点，是需要大量的技术和细节支撑，一点一点地从严苛的现实世界中，从每天24小时里面，取回自己想要的东西。

在理完纷乱的俗务之后，还能心向茫茫的星辰大海。


# 其 二

这五年来，交流上的改进是另一个非常重要的环节。

之前的《博士五年总结》介绍了博士阶段应有的交流能力，做演讲，和同行交流，论文要写得流畅，等等。这些是作为一个科研工作者的应有水准。然而要是只做到这一点，只能说是一个合格的从业者。

再往上走，就应该要知道如何通过交流，去理解别人的期望，去争取自己的利益。


## 理解别人的期望

<font color='green'>中国和美国有一点<font color='red'>非常不同</font>：中国人往往努力干活，相互谦让，遇事忍耐，心里期待着上头的领导能公平安排；美国的惯例则是<font color='red'>每个人都需要为自己说话</font>，往往在会上吵得鸡飞狗跳，直到找到大家满意的平衡点为止</font>。

中国这一套体系对领导的能力素养提出了非常高的要求，领导需要了解每个人的脾气性格，需要估计会发生的可能情况，并且及时处理，而员工可以专心完成自己的工作。美国则把责任分摊给个人进行分布式决策，只要大家都理性，都愿意为自己争取，总会有一个还不错的结果。

在美国这样的环境下，<font color='red'>交流就变得非常重要</font>。我们常觉得自己工作非常努力，周末加班无数，修完各种bug，却看见别人和老板关系好升职快，觉得自己在职场上被欺负而愤愤不平——但很多时候，并不是活干得不够多，并不是因为被人歧视，而是因为<font color='green'>没有产生有效的交流，没有找到正确的方向并且努力</font>。对公司来说，员工找到正确的方向，做正确的事情是非常重要的，这比每天工作时长要重要得多。可能打照面说个一两句话，就抵得上几周的工作和几万行代码。这可能被以工作量作为唯一准绳的人认为是作弊，但世界往往就是如此运行的。

而为了找到正确的方向，<font color='green'>需要一些“能说会道”或者说是“溜须拍马”的能力</font>，去作沟通。一个很关键的技巧是<font color='green'>不要对任何事情说“好的”，“没问题”，而是要停一下，想一想，多思考多询问，多提关键问题</font>。这样一方面可以显示出你确实理解了对方的想法，另一方面，给自己留足斡旋的空间。经常见到的例子是还没开始怎么讨论，就一个劲地说没问题包在我身上，回头发现自己没听懂要求也没想过怎么做，然后又碍于面子不敢问，这样把自己折腾得够呛，绩效又不好，责任又全在自己身上，真是亏大了。

在中国我们很少思考这样的问题，因为我们往往都<font color='green'>假定领导全知全能</font>，自己付出的努力，领导应该都看在眼里，并且及时给予公正的奖励。但现实并非如此，大家都是人，没有人多长几只眼睛，偏偏往自己这边多看两眼。把它们归为肤色问题，躲在受害者的被窝里哭一哭，说两句风凉话，心里好过是好过了，但就没办法进步了。

再往上走，<font color='green'>成为管理者之后，对交流的能力要求就更高。不仅对上对下都要理解期望，而且要从行为举止中摸清游戏规则，并且在游戏规则里为组员争取最大的权益</font>。这可以说是做管理非常枯燥的地方，但也是有意思之处。

而如何去摸清规则，就要靠交流。很多时候规则会完全出乎我们的意料，经常会有“为什么我的建议总被人否决？”“为什么他们居然提这样的项目？而上面居然会批准？”这样的疑问，有时候甚至会怀疑人生。主要原因是<font color='green'>不同职阶的人想的东西是完全不一样的</font>，往往是普通员工想升职，经理们想招更多的人扩大团队，总监们想的是如何提高项目的影响力，VP们会去想如何提升公司的品牌，维护公司的价值观，等等。这里举的例子都只是通常的思路，实际情况要复杂得多，经常有惊掉下巴的清奇脑回路出现。而每个组里每个人思路的不同，各类人的个性和人员配比的不同，会造成千变万化的游戏规则和团队微环境，必须<font color='green'>既要谨言慎行，却又要在关键时刻挺身而出</font>。要能真正做到这一点，与各个利益团队充分及深入的交流是必不可少的。如果是摸不到脉络却又随便说话，可能一两个照面就被搞下去了。

能说会道，溜须拍马的背后，是更充分的沟通；使盘外招，出无理牌的背后，是对规则更深的了解。不了解规则的时候，就应当虚心谨慎。我记得某部书里说过一句话：“<font color='red'>居上位者，定有其过人之处</font>”，深以为然。对此，我们得先存敬畏之心，后寻理解之道。而要解开这些谜团，就只有靠充分交流与认真思考。反过来，要是觉得自己很厉害，觉得大家都要听自己的，没有“我可能会犯错“的意识，结局可能就如《三体》所说——“<font color='blue'>弱小和无知不是生存的障碍，傲慢才是</font>”。


## 争取自己的利益

我们从小到大的教育，特别是历史教育，往往会让人去信奉<font color='green'>弱肉强食的达尔文主义</font>。大家常常对近代史耿耿于怀，想着落后就要挨打，想着要吃亏了要赚回来，看到赢者通吃为所欲为，看到输者只能躲在角落里瑟瑟发抖。很多小说都走这样的路子，大家看了，代入主角爽一把，然后继续这样的思维方式。其实这种理解虽然有一定道理，但太过粗糙也失之极端。人和动物一个很大的区别，在于<font color='green'>互惠互利成为主流</font>，因此制定出来的规则也更加复杂。在这种情况下，其实已经分不出绝对的强者或者弱者，有人人脉广，有人主意多，有人经验丰富，有人写代码厉害，有人文章看得深，有人交流能力强，缺一不可，大家合作，才能双赢或者多赢。

在这种情况下，<font color='green'>明白自己的地位，决定自己想要扮演什么样的角色，并努力争取相应的权益，就是理所当然的事情</font>。这样就要具体问题具体分析，在不同情况采用不同的策略。按照这样的逻辑，一个人的处世哲学，也无法用几个简单的词去概括，不管是“无私奉献”或者是“明哲保身”还是“闷声发大财”，都无法成为事前奉行的准则，而更多是事后的标签和注脚。

在决定了自己的角色之后，当然<font color='green'>要尽职尽责，但同时也要尽力争取回报</font>。可能是因为有长期互撕的历史，美国很多细节都按照权责分明的方案设计，<font color='red'>义务和利益是对等的</font>，能站出来挑大梁，做成了就有好的回报。想负责一个大项目？给人给资源给时间，而压力马上就会来。我们的ELF OpenGo项目拿到很多资源，但与此同时每三天就要写一个进度报告。而一旦有了结果，那就有机会给上面汇报和展示工作，提高知名度和影响力。像这样露脸的机会，就要好好抓住，把平时演讲的功夫全都展示出来；写总结的时候也要如实写明，若是自己都谦虚退让，那还有谁可以替着说话？
- 负责招人？可以拍板，但得陈述理由，仔细分析个人面试意见，还要接受各种询问、异议甚至是质问。更大的责任是招完人还要负责对方的职业发展，经常听到的问题是“你招进来，你带不带？”像这种单刀直入的问题，基本上杜绝了随便招人的现象，更不用说关系户了。这一块往往殚精竭虑，因为风险是相当大的——招个牛人事半功倍；反之若是招人不慎，给组里制造各种混乱，那得要脱掉半层皮了。
- 想管理团队？那得要负责把握方向，定下目标，花时间去开会去沟通，<font color='green'>给组员们以各种方便，还要在评定会上公平公正地讲述组员的贡献，找到各种理由给他们更好的待遇。如果自己的组员做得比别人好，那就找各种攻击点据理力争，直到大家相互间找到各自有利的地方妥协为止</font>。<font color='red'>这无关道德品行，纯粹是管理者的义务</font>。若是开会时畏畏缩缩，那为什么大家还要跟着你？能吵架，才有尊重；而获得尊重，对于管理者本身就是回报的一部分。
   - 我以前实习时的老板从微软跳入了我们的组，一开始他是我的老板，后来成为同事，然后在会上为各自组里的利益争得面红耳赤，开完会他对我说干得好（good job）——在那一刻，我觉得这是对自己最大的褒奖。


# 其 三

从小到大，常听长辈们说：“<font color='blue'>好好读书，好好学习，长大了才有好工作</font>。”，似乎只要努力十几年，接下来就如童话般有一个美好的结局。但等我们真的到了而立之年，真的去找了一份工作，才发现人生才刚刚开始，接下来要怎么成长，怎么发展，长辈们已经没有故事再编给我们听了。

工作之后，很容易进入<font color='green'>舒适区和回音室（echo chamber）</font>，每天挤同样的地铁，处理同样的事务，聊熟悉的话题，结婚生娃养娃，不知不觉间韶华已逝，直到退休老死。有句话说的好：“<font color='red'>有的人25岁就死了，75岁才埋</font>”，现在的人平均寿命已经超过75了，要是刚开始工作就看完了一生，那还有一多半时间要如何处理呢？

若是想要继续成长，那就不得不发挥更多的主观能动性了。


## 走出舒适区和离开回音室

“<font color='green'>走出舒适区</font>”这五个字，我想大家在各类鸡汤文里面看到很多次了，无非是不甘平庸，要努力，要抓紧时间提升自己，要尝试新事物。我在之前的博文里面也提过一些，这里也不想多说了。

但“<font color='green'>离开回音室</font>”，却鲜少有人提及。

从小到大，外界的反馈信号是越来越弱的。上大学前有老师和家长管头管脚，本科硕士时有师兄师姐，博士时还有导师。工作了之后，中肯有益的批评会越来越少，大家都忙于自己的工作，没有空也没有义务去评论别人，能得到的评论都是“还不错啊”，“挺好的”，“很厉害”。而有一两条不顺眼的评价，或许拉黑关评论也就可以了，再没有强制听取的必要了，久而久之，能听到的都只是赞美了。

然而，<font color='green'>别人的反馈是一面镜子，能照到自己看不到的角落</font>。一般足够勤奋足够用心的人，很多时候并不是因为没达到预定的目标，而是有意无意地忽略了其它的方向。很多时候，不将棋盘掀掉再来想问题，问题是无解的；而掀桌子的那些人，往往是门清的旁观者，他们会残酷地把筋疲力尽的奋斗者拖起来，撕开“勤奋向上”的保护罩，在他的耳边吼道：你的路子是错的，迄今为止的所有努力都是毫无意义的。

<font color='green'>工作了，真心的批评会越来越少，而噪音则会越来越多</font>。有时候可能皱个眉头或者话里有话，几秒钟的事情，态度就已经说出来了；有时候被人无端贬损，其实对方只是发泄情绪，自己并未做错；更高段的是出于各自利益相互攻击。这时候能不能把得住舵，既能听从别人的意见完善自己，又过滤不实之辞予以反击，就是个很难的事情，精神不够强韧的人，往往就躲到回音室里再不出来了。这里面的<font color='green'>“度”很难控制，看得准不准，解读得对不对，一是取决于自己的阅历，二是要多方了解再做评判</font>。一个受着冒名顶替综合症（Impostor syndrome）困扰的人，可能一两句随便的批评就会被毁自信，这时候多听听各种人的意见，同时把自己的功绩和自己的努力联系起来，再建立不依赖于别人的自我评价体系，会在很大程度上平衡这种困扰。

<font color='green'>听取别人的意见，并不只是说做做表面文章</font>。我们常说谦虚是一种美德，虚怀若谷是一种君子气度，这种说法其实是很肤浅的——也许自我感觉良好，别人不过尔尔，但碍于面子让一让也无妨。这种假谦虚是无法改变自己的，只会通过暗地贬损别人的方式来加强回音室的效果。只有意识到自己可能真是错的，也许事情真不是所想的那样，这样在看到清楚的逻辑和充足的证据后，才能听进去。

<font color='green'>多听别人的意见，但要记得选择权在自己</font>。自己出了错不要全怪他人，不要怪老师怪师兄怪父母，那样只会让自己在弱者的循环里越陷越深。自己的路是自己走的，盲听盲从是自己的失误，去伪存真是自己的责任。把锅全都甩给别人的那一刻，虽然获得了一时的轻松，却也在不知不觉间，停下前进的脚步。

<font color='green'>最后，不要太过经常去评判别人</font>。<font color='blue'>大家做事都有背后的逻辑，或者有不得已的苦衷，拿自身的幸运去对比别人的苦难，既不仁也不智</font>。而更重要的是，经常评判别人会陷入强者的幻想中，浑身舒坦，似乎自己全知全能，别人错漏百出，久而久之，每天沐浴在和煦的阳光下，只聆听脑海中的掌声，那又陷入回音室的陷阱里面去了。相比之下，忙着赶路的人，更关注自己要面对要解决的问题，往往对别人更宽容---<font color='red'>只要别人不挡着道，他们做什么和我有什么关系呢？</font>


## 选一个好的环境

虽然个人因素是很重要的，但环境因素也不可或缺。

一个好的环境和平台，首先给人的是成长空间。有句话叫<font color='green'>宁作鸡头不当凤尾</font>。当了鸡头，最重要的是有了责任感，事情得要好好想，决策得要好好做，做对了有成就感，做错了有切肤之痛。处于这种状态，压力会大，但不知不觉会进步，会想办法更好地分析问题。反之要是凡事都有上头罩着，一举一动都不需要自己负责，那不出半年人就会退化，再要赶上就难了。

回想读博本身，就是找了一个很好的平台。周围都是懂行的人，提出的问题往往一针见血切中要害。而工作了之后，周围的人的背景变杂了，往往你是在本公司里最懂某一块的人，其它人也没能力也没必要挑战权威。这时候找一些同道经常交流，就是很有必要的了。

另一个是格局要大，或者有能变大的潜力。天天为代码缩进一个空格还是两个空格而讨论，或者重复机器能做的工作，或只在原有的平台上修补bug，或者总做小事情，得不到上头的信任和委托，那年薪百万都是没有意义的。更大格局也会降低以后被自动工具替代的风险。人工智能的进步是很快的，看着海水很平静，待到涨潮漫上滩涂时，再往山上跑就晚了。


## 平衡收入和可持续的职业发展

如果说有什么地方我的观念和读博时截然相反，那可能是对金钱的态度上。学生的时候觉得钱不重要，理想最重要，为了理想光荣，沾了铜臭可耻。

事实上不得不说<font color='red'>这个世界是由经济和交换驱动的</font>，即便是去当教授，也一样要写各种grant和各种proposal，为自己的利益争得头破血流。除开“挣得面子”或是“获得安全感”这些，有一个好的经济基础，对我更重要的是有两点。
- 首先<font color='green'>收入的多少是一种筛选机制</font>。虽然牛人不都是冲着钱而来，但总的来说，还是工资高的地方牛人较多，无他，每个人都有现实的压力，能够获取高工资也是实力的证明，对将来有好处。更重要的一点是，有钱可以节省大量花费在琐事上的时间。没有足够的收入，就不得不去讨论和争执一些平凡的事务，这样会耗费掉大量资源，不管是时间资源还是情感资源。这种耗费，反过来会把人一直锁在糟糕的状态下而无法成长。就算自己不问世事，对小事漠不关心或是逃避，也会有各种外界因素逼得自己去处理和面对。这种被动的推动，是不愉快的主要缘由之一。与其处处被动，不如找到一份有足够回报的，“体面”的工作。钱确实买不来时间，但钱可以帮你打消掉很多琐事，也是在很多情况下说服亲戚和长辈们的唯一办法，这样才可以把有限的时间和精力集中在更重要的事情上面。
- 另一方面，<font color='green'>如果找工作只看钱，则是走进了另一个极端里面了</font>。市场是在不断变化的，今年可能前端做网页火了，明年是虚拟现实元年，后年人工智能迎来一波大热潮，再后来区块链成新宠，如果每次只找挣钱最多的方向，频繁跳槽，那肯定会中断自己职业发展的历程。从长远看，有一个自己喜爱的方向，在专业上有长期的积累，才能越老越香。人生这场马拉松，其实一开始跑得快点或者慢点没太大关系，重要的是能不能一直朝着某个方面坚持，一直往前走。现在知识不再被垄断，网上材料那么多，想学什么都是容易的，只要愿意，弱点总是可以被弥补的。

这里举个计算机的例子。我们经常听到两种截然不同的说法，一说<font color='green'>计算机是青春饭，三十岁以后就做不动了</font>；另一说<font color='green'>资深员工很受欢迎</font>。到底哪句话是对的？很简单，<font color='red'>职业发展越往后，人和人之间的差距越大，年龄得要和水平成正比</font>。小年轻只要聪明努力肯干，给个入门职位招进来不是问题；一旦年岁渐涨，招人就要考虑投资回报比的问题。有些资深员人能带人管人，关键时刻以一当十，公司不付几倍工资出来，真是不好意思；有些所谓资深员工，水平也就和刚入门的人差不多，那为什么还要花更多的钱呢？

在这点上，<font color='green'>计算机这个方向其实比很多其它方向好得多</font>，因为一个牛人往往顶十人甚至百人，一个水平不高的人在团队里会产生巨大的反效果，所以<font color='red'>人和人的区分度很大</font>，牛人的价值要高得多。要是大家都只能干些搬砖的活，那么人和人的差别就不大了——在这种情况下，差别一定还是会有，但就不得不体现在技术之外了，这一点我相信大家都会有体会，我就不谈了。

总之，找到自己的位置并且充分发挥自己的特长，然后适当地和现实妥协，我觉得会是比较可行的方案，至于什么可以妥协，什么需要坚持，则是见仁见智的事情，需要具体问题具体分析。在此我很推荐科研的道路，这份职业没有通常工种“望尽天涯路”的厌倦无聊，只要思想开明就有各种新奇东西可以玩一辈子，只要努力那么能取得的成就上不封顶，工资也足够养家，在新技术的浪潮面前有较少被替代的风险——科研本身，就是开启和推动浪潮的原动力。

而科学家所带来的，乃是我们这些渺小的尘埃，在亿万光年的冷暗宇宙中传播火种的希望。


# 我想要成长么？

说了那么多，其实成长并非每个人都想要的东西。

对大部分人而言，日复一日的安稳平淡生活，就已经很不错了。不管是辉煌的荣耀也好，平凡的生活也罢，最终不过沦为几分钟的饭后谈资，或者几秒钟的脑海片段，敬佩或是感叹、讥笑或是遗憾，喜乐或是悲哀——哪天不在了，太阳还是照常升起，而后大家该如何活就如何活，所谓“亲戚或余悲，他人亦已歌”，并非世态炎凉，确是人之常情。

所以啊，<font color='green'>是满足于岁月静好，还是决意于前行远方，都只是为个体的快乐而做出的决定罢了——而做出这个决定本身，就已经是成长的一部分了</font>。

# 补 充

最后再私心分享渊栋之前写过的特别喜欢的一段话：

>有人问我，梦想如何坚持？梦想破灭了怎么办？我想要回答，但是真要提笔的时候，又不知道如何说起。其实，这世界上没有破灭和未破灭这两种状态，没有是或非两种结论，这世界上有的，只是日升日落，人来人往。你说要有光，那就有光，光在你心里；你要追求什么，那东西就不曾离你而去；而你若忘却，它就消亡。所以，若是要坚持所谓的梦想，那么就如同小说中写的那样——

求道之人，不问寒暑。
